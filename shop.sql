/*
Navicat MySQL Data Transfer

Source Server         : hua
Source Server Version : 50553
Source Host           : localhost:3306
Source Database       : shop

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2018-03-23 17:39:40
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for shop_action
-- ----------------------------
DROP TABLE IF EXISTS `shop_action`;
CREATE TABLE `shop_action` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `title` varchar(50) NOT NULL COMMENT '动作名称',
  `rule` varchar(50) NOT NULL COMMENT '动作路径',
  `description` varchar(255) NOT NULL COMMENT '描述',
  `sort` int(12) NOT NULL DEFAULT '0' COMMENT '排序',
  `create_time` int(255) DEFAULT NULL COMMENT '创建时间',
  `update_time` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='动作表';

-- ----------------------------
-- Records of shop_action
-- ----------------------------
INSERT INTO `shop_action` VALUES ('1', '新增商品', 'back/brand/set', '这是商品管理功能', '0', '1521594960', '1521595134');
INSERT INTO `shop_action` VALUES ('2', '批量操作', 'back/brand/multi', '批量操作', '0', '1521600343', '1521600343');
INSERT INTO `shop_action` VALUES ('3', '修改产品', 'back/brand/set', '修改产品', '0', '1521600368', '1521600368');
INSERT INTO `shop_action` VALUES ('4', '品牌列表', 'back/brand/index', '品牌列表', '0', '1521619233', '1521619233');
INSERT INTO `shop_action` VALUES ('5', '删除操作', 'back/brand/multi', '删除操作', '0', '1521619256', '1521619256');
INSERT INTO `shop_action` VALUES ('6', '首页', 'back/site/index', '首页', '0', '1521619256', '1521619256');

-- ----------------------------
-- Table structure for shop_action_role
-- ----------------------------
DROP TABLE IF EXISTS `shop_action_role`;
CREATE TABLE `shop_action_role` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `action_id` int(50) NOT NULL COMMENT '动作主键',
  `role_id` int(50) NOT NULL COMMENT '角色（权限）主键',
  PRIMARY KEY (`id`),
  KEY `action_id` (`action_id`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `action_id` FOREIGN KEY (`action_id`) REFERENCES `shop_action` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `role_id` FOREIGN KEY (`role_id`) REFERENCES `shop_role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='角色—动作关联';

-- ----------------------------
-- Records of shop_action_role
-- ----------------------------
INSERT INTO `shop_action_role` VALUES ('6', '1', '1');
INSERT INTO `shop_action_role` VALUES ('8', '3', '1');
INSERT INTO `shop_action_role` VALUES ('9', '1', '2');
INSERT INTO `shop_action_role` VALUES ('10', '2', '2');
INSERT INTO `shop_action_role` VALUES ('11', '3', '2');
INSERT INTO `shop_action_role` VALUES ('12', '1', '3');
INSERT INTO `shop_action_role` VALUES ('13', '2', '3');
INSERT INTO `shop_action_role` VALUES ('14', '3', '3');
INSERT INTO `shop_action_role` VALUES ('15', '4', '2');
INSERT INTO `shop_action_role` VALUES ('16', '6', '2');
INSERT INTO `shop_action_role` VALUES ('17', '6', '1');
INSERT INTO `shop_action_role` VALUES ('18', '2', '1');
INSERT INTO `shop_action_role` VALUES ('19', '4', '1');
INSERT INTO `shop_action_role` VALUES ('20', '5', '1');

-- ----------------------------
-- Table structure for shop_admin
-- ----------------------------
DROP TABLE IF EXISTS `shop_admin`;
CREATE TABLE `shop_admin` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `username` varchar(60) NOT NULL COMMENT '用户名',
  `password` varchar(60) NOT NULL COMMENT '密码',
  `is_super` int(1) DEFAULT '0' COMMENT '超级管理员',
  `sort` int(12) NOT NULL COMMENT '排序',
  `create_time` int(50) DEFAULT NULL COMMENT '创建时间',
  `update_time` int(50) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COMMENT='管理员';

-- ----------------------------
-- Records of shop_admin
-- ----------------------------
INSERT INTO `shop_admin` VALUES ('3', 'kepa111ma', 'fcea920f7412b5da7be0cf42b8c93759', '0', '0', '1521524738', '1521680490');
INSERT INTO `shop_admin` VALUES ('4', 'ksdasd', 'd9a07834404334b0e4406138d3da0f54', '0', '0', '1521525420', '1521525420');
INSERT INTO `shop_admin` VALUES ('5', 'admin', 'admin', '1', '2', '1521524738', '1521701941');
INSERT INTO `shop_admin` VALUES ('6', 'asdsads', 'e10adc3949ba59abbe56e057f20f883e', '0', '0', '1521620519', '1521620519');
INSERT INTO `shop_admin` VALUES ('7', 'asdasdad', 'e10adc3949ba59abbe56e057f20f883e', '0', '0', '1521620546', '1521620546');
INSERT INTO `shop_admin` VALUES ('8', 'papapapa', 'cc3015504e6fba9161ede5f1666120ea', '0', '0', '1521620741', '1521638660');
INSERT INTO `shop_admin` VALUES ('9', 'dsanww222', 'cc3015504e6fba9161ede5f1666120ea', '0', '0', '1521620851', '1521620851');
INSERT INTO `shop_admin` VALUES ('10', 'balalalas', 'e10adc3949ba59abbe56e057f20f883e', '0', '0', '1521624760', '1521624760');
INSERT INTO `shop_admin` VALUES ('11', 'asdasdad123', '25d55ad283aa400af464c76d713c07ad', '0', '0', '1521635232', '1521635232');
INSERT INTO `shop_admin` VALUES ('12', 'asddsaasd', '1bbd886460827015e5d605ed44252251', '0', '0', '1521635274', '1521635274');
INSERT INTO `shop_admin` VALUES ('13', 'asdasdad1234', '25d55ad283aa400af464c76d713c07ad', '0', '0', '1521635325', '1521635325');
INSERT INTO `shop_admin` VALUES ('14', 'qwe123123123', 'f5bb0c8de146c67b44babbf4e6584cc0', '0', '0', '1521638695', '1521638695');
INSERT INTO `shop_admin` VALUES ('15', 'qwe1231222', 'f5bb0c8de146c67b44babbf4e6584cc0', '0', '0', '1521638817', '1521638817');
INSERT INTO `shop_admin` VALUES ('16', 'dsanww', 'qwertyu', '0', '0', '1521638817', '1521701280');

-- ----------------------------
-- Table structure for shop_admin_role
-- ----------------------------
DROP TABLE IF EXISTS `shop_admin_role`;
CREATE TABLE `shop_admin_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `admin_id` int(11) NOT NULL COMMENT '用户id',
  `role_id` int(11) NOT NULL COMMENT '权限id',
  PRIMARY KEY (`id`),
  KEY `admin_id` (`admin_id`),
  KEY `admin_role_id` (`role_id`),
  CONSTRAINT `admin_id` FOREIGN KEY (`admin_id`) REFERENCES `shop_admin` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `admin_role_id` FOREIGN KEY (`role_id`) REFERENCES `shop_role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8 COMMENT='用户，权限';

-- ----------------------------
-- Records of shop_admin_role
-- ----------------------------
INSERT INTO `shop_admin_role` VALUES ('4', '9', '1');
INSERT INTO `shop_admin_role` VALUES ('5', '9', '3');
INSERT INTO `shop_admin_role` VALUES ('16', '10', '1');
INSERT INTO `shop_admin_role` VALUES ('17', '10', '2');
INSERT INTO `shop_admin_role` VALUES ('18', '10', '3');
INSERT INTO `shop_admin_role` VALUES ('22', '12', '1');
INSERT INTO `shop_admin_role` VALUES ('23', '12', '3');
INSERT INTO `shop_admin_role` VALUES ('24', '13', '2');
INSERT INTO `shop_admin_role` VALUES ('25', '8', '1');
INSERT INTO `shop_admin_role` VALUES ('29', '3', '1');
INSERT INTO `shop_admin_role` VALUES ('37', '16', '1');
INSERT INTO `shop_admin_role` VALUES ('38', '5', '1');

-- ----------------------------
-- Table structure for shop_brand
-- ----------------------------
DROP TABLE IF EXISTS `shop_brand`;
CREATE TABLE `shop_brand` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `title` varchar(50) DEFAULT NULL COMMENT '标题',
  `logo` varchar(200) DEFAULT NULL COMMENT 'LOGO',
  `site` varchar(50) DEFAULT NULL COMMENT '网站',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of shop_brand
-- ----------------------------
INSERT INTO `shop_brand` VALUES ('12', '老司机信赖合作产品', '司机a.png', 'http://www.olddriver.com', '4', '1521006526', '1521007253');
INSERT INTO `shop_brand` VALUES ('11', '322222222', '4', 'http://www.baidu.com', '2', '1520996520', '1520996520');
INSERT INTO `shop_brand` VALUES ('6', 'Adidas王', 'Adidas.png', 'http://www.adidas.cn', '2', '1520844785', '1521006487');
INSERT INTO `shop_brand` VALUES ('7', '汇源100%果汁', '123', 'http://www.baidu.com', '1', '1520907931', '1520907931');
INSERT INTO `shop_brand` VALUES ('8', '100年洗发露', '1', 'http://www.baidu2.com', '3', '1520907950', '1520907950');

-- ----------------------------
-- Table structure for shop_category
-- ----------------------------
DROP TABLE IF EXISTS `shop_category`;
CREATE TABLE `shop_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `title` varchar(255) NOT NULL COMMENT '分类',
  `parent_id` int(11) NOT NULL COMMENT '上级分类',
  `sort` int(11) DEFAULT '0' COMMENT '排序',
  `is_used` int(11) DEFAULT '0' COMMENT '启用',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='分类';

-- ----------------------------
-- Records of shop_category
-- ----------------------------
INSERT INTO `shop_category` VALUES ('1', '未分类', '0', '0', '0', '1521770583', '1521770583');
INSERT INTO `shop_category` VALUES ('2', '物理', '1', '0', '0', '1521770605', '1521770605');
INSERT INTO `shop_category` VALUES ('3', '法术', '1', '0', '0', '1521770612', '1521784313');
INSERT INTO `shop_category` VALUES ('4', '用刀者', '2', '0', '0', '1521770646', '1521770646');
INSERT INTO `shop_category` VALUES ('5', '用剑者', '2', '0', '0', '1521770668', '1521784480');
INSERT INTO `shop_category` VALUES ('6', '魔法杖', '3', '0', '0', '1521770676', '1521784458');
INSERT INTO `shop_category` VALUES ('7', '魔法宝典', '3', '0', '0', '1521770685', '1521770685');
INSERT INTO `shop_category` VALUES ('8', '混合', '1', '0', '0', '1521784504', '1521784504');
INSERT INTO `shop_category` VALUES ('9', '神圣', '1', '0', '0', '1521784513', '1521784513');
INSERT INTO `shop_category` VALUES ('10', 'ex咖喱棒', '8', '0', '0', '1521784648', '1521784648');
INSERT INTO `shop_category` VALUES ('11', '泉水之眼', '9', '0', '0', '1521784674', '1521784674');

-- ----------------------------
-- Table structure for shop_member
-- ----------------------------
DROP TABLE IF EXISTS `shop_member`;
CREATE TABLE `shop_member` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(255) DEFAULT NULL COMMENT '用户名',
  `mark` varchar(255) DEFAULT NULL COMMENT '备注',
  `sort` varchar(255) DEFAULT NULL COMMENT '排序',
  `create_time` int(255) DEFAULT NULL COMMENT '添加时间',
  `update_time` int(255) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='自动化生成';

-- ----------------------------
-- Records of shop_member
-- ----------------------------
INSERT INTO `shop_member` VALUES ('1', 'asd', '233', '2', '1521438766', '1521438766');
INSERT INTO `shop_member` VALUES ('2', 'basd', '23', '1', '1521438766', '1521438766');
INSERT INTO `shop_member` VALUES ('3', 'balal', 'gogogo', null, '1521443738', '1521443738');
INSERT INTO `shop_member` VALUES ('4', 'das', 'asdas', 'asd', '1521438766', '1521438766');
INSERT INTO `shop_member` VALUES ('5', 'das', 'asdas', 'asd', '1521438766', '1521438766');
INSERT INTO `shop_member` VALUES ('6', 'das', 'asdas', 'asd', '1521438766', '1521438766');
INSERT INTO `shop_member` VALUES ('7', 'das', 'asdas', '1', '1521438766', '1521438766');
INSERT INTO `shop_member` VALUES ('8', 'das', 'asdas', '1', '1521438766', '1521438766');
INSERT INTO `shop_member` VALUES ('9', 'gande66', 'kepa', 'a', '1521444013', '1521444013');

-- ----------------------------
-- Table structure for shop_role
-- ----------------------------
DROP TABLE IF EXISTS `shop_role`;
CREATE TABLE `shop_role` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `title` varchar(50) NOT NULL COMMENT '角色',
  `is_super` int(1) NOT NULL DEFAULT '0' COMMENT '是否超级管理员',
  `description` varchar(50) DEFAULT NULL COMMENT '描述',
  `sort` int(12) NOT NULL DEFAULT '0' COMMENT '排序',
  `create_time` int(50) DEFAULT NULL COMMENT '创建时间',
  `update_time` int(50) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='角色表';

-- ----------------------------
-- Records of shop_role
-- ----------------------------
INSERT INTO `shop_role` VALUES ('1', 'root', '0', '这是管理员', '1', '1521594814', '1521595046');
INSERT INTO `shop_role` VALUES ('2', '品牌管理员', '0', '品牌管理员', '0', '1521618678', '1521618678');
INSERT INTO `shop_role` VALUES ('3', '销售商', '0', '销售商', '0', '1521618830', '1521618830');
INSERT INTO `shop_role` VALUES ('4', '中介商', '0', '中介商', '0', '1521618912', '1521618912');
