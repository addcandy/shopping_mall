<?php
/**
 * Created by PhpStorm.
 * User: 王世鹏
 * Date: 2018/3/28
 * Time: 13:03
 */
namespace app\api\controller;
use app\api\model\Product;
use \think\Controller;
class ProductController extends Controller
{
    public function listAction()
    {
        $model = new Product();

        $limit = input("limit",4);

        $type = input("type","all");
        $status = 2;
        switch ($type){
            case "new":
                $status = 1;
                break;
            case "old" :
                $status = 0;
                break;
            default:
                break;
        }
        if($status==2){
            $result = $model->select();
        }
        else{
            $result = $model->where([
                "status" =>$status
            ])->paginate($limit);

        }


        return json($result);
    }

}