<?php
/**
 * Created by PhpStorm.
 * User: 王世鹏
 * Date: 2018/3/12
 * Time: 14:38
 */

namespace app\back\validate;


use think\Validate;

class Brand extends Validate
{
    protected $rule = [
        "title"    => "require|max:20|min:6",
        "sort"    => "require|integer",
        "site"     =>"url",
        "logo" =>"require"
    ];

    protected $field = [
        "title"   => "商品",
        "sort"   => "分类",
        "logo"   => "LOGO",
        "site"    => "域名"
    ];

}