<?php

namespace app\back\validate;


use think\Validate;

class ActionValidate extends Validate
{
    protected $rule = [
        #自定义规则
    ];

    protected $field = [
        

        "id" =>"主键",

        "title" =>"动作名称",

        "rule" =>"动作路径",

        "description" =>"描述",

        "sort" =>"排序",

        "create_time" =>"创建时间",

        "update_time" =>""
    ];

}