<?php

namespace app\back\validate;


use think\Validate;

class RoleValidate extends Validate
{
    protected $rule = [
        #自定义规则
    ];

    protected $field = [
        

        "id" =>"主键",

        "title" =>"角色",

        "is_super" =>"是否超级管理员",

        "description" =>"描述",

        "sort" =>"排序",

        "create_time" =>"创建时间",

        "update_time" =>"更新时间"
    ];

}