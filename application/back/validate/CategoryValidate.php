<?php

namespace app\back\validate;


use think\Validate;

class CategoryValidate extends Validate
{
    protected $rule = [
        #自定义规则
    ];

    protected $field = [
        

        "id" =>"主键",

        "title" =>"分类",

        "parent_id" =>"上级分类",

        "sort" =>"排序",

        "is_used" =>"启用",

        "create_time" =>"创建时间",

        "update_time" =>"更新时间"
    ];

}