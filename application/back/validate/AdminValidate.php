<?php

namespace app\back\validate;


use think\Validate;

class AdminValidate extends Validate
{
    protected $rule = [
        "username" =>"require|max:16|min:3|unique:admin",
        "password" =>"require|max:16|min:3",
        "password_confirm" =>"require|confirm:password"
    ];

    protected $field = [
        

        "id" =>"id",

        "username" =>"用户名",

        "password" =>"密码",

        "sort" =>"排序",

        "create_time" =>"创建时间",

        "update_time" =>"更新时间",
        "password_confirm"=>"确认密码"
    ];

    protected $scene = [
            "update" =>["username"],
            "repassword"=>["password","password_confirm"],
    ];

}