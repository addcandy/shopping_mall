<?php

namespace app\back\validate;


use think\Validate;

class MemberValidate extends Validate
{
    protected $rule = [
        #自定义规则
    ];

    protected $field = [
        

        "id" =>"主键",

        "name" =>"用户名",

        "mark" =>"备注",

        "sort" =>"排序",

        "create_time" =>"添加时间",

        "update_time" =>"更新时间"
    ];

}