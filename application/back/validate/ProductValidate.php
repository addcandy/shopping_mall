<?php

namespace app\back\validate;


use think\Validate;

class ProductValidate extends Validate
{
    protected $rule = [
        "title" =>"require",
        "inventory" =>"require|integer",
        "mininum" =>"require|integer",
        "image" =>"require"
    ];

    protected $field = [
        

        "id" =>"id",

        "title" =>"名称",

        "upc" =>"条码",

        "image" =>"缩略图",

        "inventory" =>"库存",

        "mininum" =>"最小起售",

        "price" =>"价格",

        "price_orign" =>"原价",

        "is_shopping" =>"配送支持",

        "date_avaliable" =>"起送时间",

        "status" =>"状态",

        "brand_id" =>"品牌id",

        "description" =>"描述",

        "category_id" =>"类别",

        "sort" =>"排序",

        "create_time" =>"创建时间",

        "update_time" =>"更新时间"
    ];

}