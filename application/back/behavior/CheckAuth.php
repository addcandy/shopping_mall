<?php
/**
 * Created by PhpStorm.
 * User: zhen
 * Date: 2018/3/20
 * Time: 15:27
 */

namespace app\back\behavior;


use priv\Privilege;
use think\Db;
use think\Session;

class CheckAuth
{
    public function run(&$params){

        $request = request();
        //屏蔽一下特定的action
        $ext = [
            'admin'=>['login']
        ];
        //需要判断用户当前访问是那个控制器和那个Action
        $controller =  $request->controller();
        if(isset($ext[strtolower($controller)])){

            $action = $request->action();

            if(in_array($action,$ext[strtolower($controller)])){
                return ;
            }
            else{

            }


        }



        //检测用户是否登录了
        if(!Session::has('admin')){
            //记录用户之前的URL
            Session::set('route',$request->path());
            redirect('admin/login')->send();
            die;
        }
        //判断是否为超级管理员
        $count = Db::name("admin")->where([
            "id" =>Session::get('admin.id'),
            "is_super" =>1
        ])->count();

        if($count>0){
            return true;
        }
        else{

        }
        //拥有退出权限

        if($request->path()=="back/admin/logout"){
            return true;
        }

        //判断用户权限

        if(!Privilege::checkPrivilege($request->path(),Session::get('admin.id'))){
//            Session::set('route',$request->path());
            redirect('site/index',[],302,[
                "msg" => "您没有权限"
            ])->send();
            die;
        }
        else{

        }




    }
}
