<?php
/**
 * Created by PhpStorm.
 * User: 王世鹏
 * Date: 2018/3/12
 * Time: 14:06
 */

namespace app\back\model;

use \think\Model;
use \think\Db;

class Category extends Model
{
    //树状结构
    public function getTree()
    {
        $rows = Db::name("category")->select();
        $trees = $this->tree($rows);
        return $trees;
    }

    //树递归

    public function tree($rows, $id = 0, $leavel = 0)
    {
        static $trees = [];

        foreach ($rows as $row) {
            if ($row['parent_id'] == $id) {
                $row["leavel"] = $leavel;
                $trees[] = $row;
                $this->tree($rows, $row["id"], $leavel + 1);
            }
        }
        return $trees;

    }


}