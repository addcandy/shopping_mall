<?php
/**
 * Created by PhpStorm.
 * User: 王世鹏
 * Date: 2018/3/12
 * Time: 14:06
 */
namespace app\back\model;
use \think\Model;
class Product extends Model
{
    protected function setUpcAttr($value)
    {
        if(empty($value)){
            return uniqid();
        }
       return $value;
    }
    protected function setDateAvaliableAttr($value)
    {
        return strtotime($value);
    }
    protected function getDateAvaliableAttr($value)
    {
        return date("Y-m-d H:i",$value);
    }

    protected function getStatusAttr($value)
    {
        return $value==1?"上架中":"下架中";
    }
    protected function getIsShoppingAttr($value)
    {
        return $value==1?"可配送":"不可配送";
    }

    protected function getDescription($value)
    {
        return html_entity_decode($value);
    }



}