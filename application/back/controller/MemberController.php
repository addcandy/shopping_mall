<?php

namespace app\back\controller;
use \think\Controller;
use app\back\model\Member;
use app\back\validate\MemberValidate;


class MemberController extends Controller
{


    //添加修改商品
    public function setAction()
    {
        $req = request();

        if($req->isGet()){
            $m = session("message");
            $i = session("info");
            $id =input("get.id");

            $one = Member::get($id);
            if(!(isset($m)&&isset($i))){
                $message = "";
                $info =$one;
            }
            else{
                $message = $m;
                $info =$i;
            }

            $this->assign("msg",$message);
            $this->assign("info",$info);
            $this->assign("id",$id);
            return $this->fetch();
        }
        elseif ($req->isPost()){
            $model = new Member;
            $v = new  MemberValidate;
            $data = input("post.");
            if(!$v->batch(true)->check($data)){
                return $this->redirect("set",[],202,[
                    "message"=>$v->getError(),
                    "info" => $data
                ]);
            }
            $result = null;
            if(!empty($data["id"])){
                $one = $model->find($data["id"]);
                $result = $one->save($data);
            }
            else{
                $result = $model->save($data);
            }
            if(!$result){
                return "添加失败：".$this->error();
            }
            else{
                return $this->redirect("index");
            }


        }
        else{


        }

    }


    //筛选
    public function indexAction()
    {
        $filter=input("filter/a");
        $filter_order=[];
        $m = new Member();


        
        //筛选查询字段name

        if(isset($filter["name"])){
        $str = str_replace("%","\%",$filter["name"]);
        $m->where("name","like","%".$str."%");
        $filter_order["filter[name]"] = $filter["name"];
        }

        //筛选查询字段mark

        if(isset($filter["mark"])){
        $str = str_replace("%","\%",$filter["mark"]);
        $m->where("mark","like","%".$str."%");
        $filter_order["filter[mark]"] = $filter["mark"];
        }

        //筛选查询字段sort

        if(isset($filter["sort"])){
        $str = str_replace("%","\%",$filter["sort"]);
        $m->where("sort","like","%".$str."%");
        $filter_order["filter[sort]"] = $filter["sort"];
        }


        $order = input("order/a");
        if(isset($order)){
            $m->order([$order["field"] =>$order["type"]]);
        }
        $size =5;
        $list = $m->paginate($size,false,['query' => request()->param()]);
        $start = $size*($list->currentPage()-1)+1;
//        $end =min($list->total(),$list->currentPage()*$size);
        $end = $start+count($list)-1;
        $this->assign("start",$start);
        $this->assign("end",$end);
        $this->assign("list",$list);
        $this->assign("filter",$filter);
        $this->assign("order",$order);
        $this->assign("filter_order",$filter_order);
        return $this->fetch();
    }




    //删除

    public function multiAction()
    {
        $data = input("selected/a");
        if(count($data)==0) $this->redirect("index");

        Member::destroy($data);
        $this->redirect("index");
    }




}





