<?php

namespace app\back\controller;
use \think\Controller;
use app\back\model\Role;
use app\back\validate\RoleValidate;
use app\back\model\Action;
use think\Db;


class RoleController extends Controller
{


    //添加修改商品
    public function setAction()
    {
        $req = request();

        if($req->isGet()){
            $m = session("message");
            $i = session("info");
            $id =input("id");

            $one = Role::get($id);
            if(!(isset($m)&&isset($i))){
                $message = "";
                $info =$one;
            }
            else{
                $message = $m;
                $info =$i;
            }

            $this->assign("msg",$message);
            $this->assign("info",$info);
            $this->assign("id",$id);
            return $this->fetch();
        }
        elseif ($req->isPost()){
            $model = new Role;
            $v = new  RoleValidate;
            $data = input("post.");

            if(!$v->batch(true)->check($data)){
                return $this->redirect("set",[],202,[
                    "message"=>$v->getError(),
                    "info" => $data
                ]);
            }
            $result = null;
            if(!empty($data["id"])){
                $one = $model->find($data["id"]);
                $result = $one->save($data);
            }
            else{
                $result = $model->save($data);
            }
            if(!$result){
                return "添加失败：".$this->error();
            }
            else{
                return $this->redirect("index");
            }


        }
        else{


        }

    }


    //筛选
    public function indexAction()
    {
        $filter=input("filter/a");
        $filter_order=[];
        $m = new Role();


        
        //筛选查询字段title

        if(isset($filter["title"])){
        $str = str_replace("%","\%",$filter["title"]);
        $m->where("title","like","%".$str."%");
        $filter_order["filter[title]"] = $filter["title"];
        }

        //筛选查询字段description

        if(isset($filter["description"])){
        $str = str_replace("%","\%",$filter["description"]);
        $m->where("description","like","%".$str."%");
        $filter_order["filter[description]"] = $filter["description"];
        }


        $order = input("order/a");
        if(isset($order)){
            $m->order([$order["field"] =>$order["type"]]);
        }
        $size =5;
        $list = $m->paginate($size,false,['query' => request()->param()]);
        $start = $size*($list->currentPage()-1)+1;
//        $end =min($list->total(),$list->currentPage()*$size);
        $end = $start+count($list)-1;
        $this->assign("start",$start);
        $this->assign("end",$end);
        $this->assign("list",$list);
        $this->assign("filter",$filter);
        $this->assign("order",$order);
        $this->assign("filter_order",$filter_order);
        return $this->fetch();
    }




    //删除

    public function multiAction()
    {
        $data = input("selected/a");
        if(count($data)==0) $this->redirect("index");

        Role::destroy($data);
        $this->redirect("index");
    }


    //权限管理

    public function setactionAction()
    {
        $req = request();
        $id = input("id");
        $one = Role::get($id);
        $action_list = Action::all();
        $check_actions =Db::name("action_role")->where("role_id",$id)->column("action_id");
        if($req->isGet()){
            $this->assign("role",$one);
            $this->assign("action_list",$action_list);
            $this->assign("check_actions",$check_actions);
            $this->assign("id",$id);

            return $this->fetch();
        }
        elseif ($req->isPost()){
            $post_actions = is_null(input("actions/a")) ? [] :input("actions/a");
            $del_actions = array_diff($check_actions,$post_actions);
            Db::name("action_role")
                ->where("role_id",$id)
                ->where("action_id","in",$del_actions)->delete();
            $insert_actions = array_diff($post_actions,$check_actions);
            if(empty($del_actions)&&empty($insert_actions)){
                $this->redirect("role/index");

            }
            $insert_actions = array_map(function($param) use ($id){
                return [
                    "role_id" =>$id,
                    "action_id" => $param
                ];
            },$insert_actions);
            Db::name("action_role")->insertAll($insert_actions);
            $this->redirect("role/index");
        }
        else{

        }
    }

}





