<?php
/**
 * Created by PhpStorm.
 * User: 王世鹏
 * Date: 2018/3/12
 * Time: 9:58
 */
namespace app\back\controller;
use \think\Controller;
use app\back\model\Brand as BrandModel;
use app\back\validate\Brand as BrandValidate;


class BrandController extends Controller
{


    //添加修改商品
    public function setAction()
    {
        $req = request();

        if($req->isGet()){
            $m = session("message");
            $i = session("info");
            $id =input("get.id");

            $one = BrandModel::get($id);
            if(!(isset($m)&&isset($i))){
                $message = "";
                $info =$one;
            }
            else{
                $message = $m;
                $info =$i;
            }

            $this->assign("msg",$message);
            $this->assign("info",$info);
            $this->assign("id",$id);
            return $this->fetch();
        }
        elseif ($req->isPost()){
            $model = new BrandModel();
            $v = new BrandValidate();
            $data = $req->post();

            //上传图片
            $data["logo"] = $req->file("logo");

            if($data["logo"]){
                $info = $data["logo"]->move(ROOT_PATH."public/upload/brand");
                $data["logo"] = $info->getSaveName();
            }

            if(!$v->batch(true)->check($data)){
                return $this->redirect("set",[],202,[
                    "message"=>$v->getError(),
                    "info" => $data
                ]);
            }


            $result = null;
            if(!empty($data["id"])){
                $one = $model->find($data["id"]);
                $result = $one->save($data);
            }
            else{
                $result = $model->save($data);
            }
            if(!$result){
                return "添加失败：".$this->error();
            }
            else{
                return $this->redirect("index");
            }


        }
        else{


        }

    }


    //筛选
    public function indexAction()
    {


        $filter=input("filter/a");
        $filter_order=[];
        $m = new BrandModel();

        if(isset($filter["title"])){
            $str = str_replace("%","\%",$filter["title"]);
            $m->where("title","like","%".$str."%");
            $filter_order["filter[title]"] = $filter["title"];

        }
        if(isset($filter["site"])){
            $str = str_replace("%","\%",$filter["site"]);
            $m->where("site","like","%".$str."%");
            $filter_order["filter[site]"] = $filter["site"];


        }
        $order = input("order/a");
        if(isset($order)){
            $m->order([$order["field"] =>$order["type"]]);
        }

        $size =5;
        $list = $m->paginate($size,false,['query' => request()->param()]);
        $start = $size*($list->currentPage()-1)+1;
//        $end =min($list->total(),$list->currentPage()*$size);
        $end = $start+count($list)-1;
        $this->assign("start",$start);
        $this->assign("end",$end);
        $this->assign("list",$list);
        $this->assign("filter",$filter);
        $this->assign("order",$order);
        $this->assign("filter_order",$filter_order);
        return $this->fetch();
    }




    //删除

    public function multiAction()
    {
        $data = input("selected/a");
        if(count($data)==0) $this->redirect("index");

        BrandModel::destroy($data);
        $this->redirect("index");
    }




}





