<?php
/**
 * Created by PhpStorm.
 * User: 王世鹏
 * Date: 2018/3/20
 * Time: 15:13
 */

namespace app\back\controller;


use think\Controller;

class SiteController extends Controller
{
    public function indexAction()
    {
        return $this->fetch();
    }

}