<?php

namespace app\back\controller;
use \think\Controller;
use app\back\model\%model%;
use app\back\validate\%validate%;


class %controller% extends Controller
{


    //添加修改商品
    public function setAction()
    {
        $req = request();

        if($req->isGet()){
            $m = session("message");
            $i = session("info");
            $id =input("id");

            $one = %model%::get($id);
            if(!(isset($m)&&isset($i))){
                $message = "";
                $info =$one;
            }
            else{
                $message = $m;
                $info =$i;
            }

            $this->assign("msg",$message);
            $this->assign("info",$info);
            $this->assign("id",$id);
            return $this->fetch();
        }
        elseif ($req->isPost()){
            $model = new %model%;
            $v = new  %validate%;
            $data = input("post.");

            if(!$v->batch(true)->check($data)){
                $data["id"] = isset($data["id"]) ? $data["id"] : null;
                return $this->redirect("set",[["id" => $data["id"]],202,[
                    "message"=>$v->getError(),
                    "info" => $data
                ]);
            }
            $result = null;
            if(!empty($data["id"])){
                $one = $model->find($data["id"]);
                $result = $one->save($data);
            }
            else{
                $result = $model->save($data);
            }
            if(!$result){
                return "添加失败：".$this->error();
            }
            else{
                return $this->redirect("index");
            }


        }
        else{


        }

    }


    //筛选
    public function indexAction()
    {
        $filter=input("filter/a");
        $filter_order=[];
        $m = new %model%();


        %where%

        $order = input("order/a");
        if(isset($order)){
            $m->order([$order["field"] =>$order["type"]]);
        }
        $size =5;
        $list = $m->paginate($size,false,['query' => request()->param()]);
        $start = $size*($list->currentPage()-1)+1;
//        $end =min($list->total(),$list->currentPage()*$size);
        $end = $start+count($list)-1;
        $this->assign("start",$start);
        $this->assign("end",$end);
        $this->assign("list",$list);
        $this->assign("filter",$filter);
        $this->assign("order",$order);
        $this->assign("filter_order",$filter_order);
        return $this->fetch();
    }




    //删除

    public function multiAction()
    {
        $data = input("selected/a");
        if(count($data)==0) $this->redirect("index");

        %model%::destroy($data);
        $this->redirect("index");
    }




}





