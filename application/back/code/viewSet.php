
{extend name="common/layout" /}

{block name="title"}%label%{if !empty($id)}修改{else/}添加{/if}{/block}
{block name="content"}
<div id="content">

    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="form-create" data-toggle="tooltip" title="保存" class="btn btn-primary">
                    <i class="fa fa-save"></i>
                </button>
                <a href="{:url('index')}" data-toggle="tooltip" title="取消" class="btn btn-default">
                    <i class="fa fa-reply"></i>
                </a>
            </div>
            <h1>%label%{if !empty($id)}修改{else/}添加{/if}</h1>
            <ul class="breadcrumb">
                <li>
                    <a href="/back/site/index.html">首页</a>
                </li>
                <li>
                    <a href="/back/brand/index.html">%label%</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <i class="fa fa-pencil"></i>
                    %label%{if !empty($id)}修改{else/}添加{/if}
                </h3>
            </div>
            <div class="panel-body">
                <form action="{:url('set')}" method="post" enctype="multipart/form-data" id="form-create"
                      class="form-horizontal">
                    {if !empty($id)}<input type="hidden" value="{$id}" name="id" />{/if}

                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#tab-general" data-toggle="tab">基本信息</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab-general">

                            %set_list%

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
{/block}
