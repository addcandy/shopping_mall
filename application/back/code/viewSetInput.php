        <div class="form-group %require%">
            <label class="col-sm-2 control-label" for="input-%field%">%label%</label>
            <div class="col-sm-10">
                <input type="text" name="%field%" value="{$info.%field%|default=''}" placeholder="%label%"
                       id="input-%field%" class="form-control"/>
                {present name="msg.%field%"}
                <label class="text-danger" id="input-%field%-error" for="input-%field%">{$msg.%field%}</label>
                {/present}

            </div>
        </div>