<?php

namespace app\back\validate;


use think\Validate;

class %validate% extends Validate
{
    protected $rule = [
        #自定义规则
    ];

    protected $field = [
        %field_list%
    ];

}