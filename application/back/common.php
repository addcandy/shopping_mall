<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用公共文件


function myUrl($route, $params=[],$order=[],$field=null){
    \think\Config::set("url_common_param",true);
    $params["order[field]"] = $field;
    $params["order[type]"] = ($order['field']==$field && $order['type']=='asc' )?'desc':'asc';
    return url($route,$params);

}


function myClassStyle($order,$field=null){
    if(!isset($order) ||$order['field']!=$field) return "";
    else{
        return  $order['type']=='asc' ?'desc':'asc';
    }

}
