<?php
/**
 * Created by PhpStorm.
 * User: WangShiPeng
 * Date: 2018/3/29
 * Time: 11:56
 */

namespace app\index\controller;


use cart\Cart;
use think\Controller;

class CartController extends Controller
{
    public function addAction()
    {
        $i = Cart::getInstance()->fetch()->add(input("product_id"),input("quantity"));

        return [
          "success"=>"ok",
            "i"=>$i
            
        ];

    }


    public function infoAction()
    {

        $info = Cart::getInstance()->fetch()->getCartlnfo();
        return json($info);
    }

}